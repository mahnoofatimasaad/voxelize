import os
import bpy
import bmesh 
from random import randint
import mathutils
import argparse


parser = argparse.ArgumentParser()
parser.add_argument("-i", "--input_path", help="Path to the input model folder containing .obj and .mtl files")
parser.add_argument("-m","--model_name", help="name of the model you want to voxelize")
parser.add_argument("-out","--output_model_name", help="name for the voxelized model to be saved as")
parser.add_argument("-o","--octree_depth", help="how many voxel for each mesh polygon (4-8 preferred)")
parser.add_argument("-s","--scale", help="size of each voxel cube 0.0-1.0")
args = parser.parse_args()

#blender loads lamp, camera and cube by default. this method deletes all objects for a clear scene
def remove_objects():
  removeThese = bpy.context.copy()
  removeThese['selected_objects'] = list(bpy.context.scene.objects)
  bpy.ops.object.delete(removeThese)



def voxelize(inputDir, modelName, outputModelName, octreeDepth, scale):
  remove_objects()
  sourceDirectory = inputDir 
  model_name = modelName + ".obj" 

  path = os.path.join(sourceDirectory, model_name)

  #loading .obj and .mtl file
  bpy.ops.import_scene.obj(filepath=path, axis_forward='-Z', axis_up='Y', filter_glob="*.obj;*.mtl")

  model_obj = bpy.context.scene.objects[model_name.split(".")[0]]       
  bpy.ops.object.select_all(action='DESELECT') 
  bpy.context.view_layer.objects.active = model_obj  
  model_obj.select_set(True)       

  sourceName = bpy.context.object.name
  source = bpy.data.objects[sourceName]

  #duplicating source model
  bpy.ops.object.duplicate_move(OBJECT_OT_duplicate={"linked":False, "mode":'TRANSLATION'})
  duplicate_obj = bpy.context.scene.objects[model_name.split(".")[0]+".001"]     
  bpy.ops.object.select_all(action='DESELECT') 
  bpy.context.view_layer.objects.active = duplicate_obj   
  duplicate_obj.select_set(True)   

  #setting output model name
  bpy.context.object.name = outputModelName     #sourceName + "_Voxelized"
  bpy.ops.object.transform_apply(location=False, rotation=True, scale=False)
  bpy.ops.object.convert(target='MESH')
  targetName = bpy.context.object.name
  target = bpy.data.objects[targetName]


  #converting to blocks
  bpy.ops.object.modifier_add(type='REMESH')
  bpy.context.object.modifiers["Remesh"].mode = 'BLOCKS'
  bpy.context.object.modifiers["Remesh"].octree_depth = octreeDepth
  bpy.context.object.modifiers["Remesh"].scale = scale
  bpy.context.object.modifiers["Remesh"].use_remove_disconnected = True
  bpy.ops.object.modifier_apply(modifier="Remesh")


  #transferring UVs from source to target
#   bpy.ops.object.modifier_add(type='DATA_TRANSFER')
#   bpy.context.object.modifiers["DataTransfer"].use_loop_data = True
#   bpy.context.object.modifiers["DataTransfer"].data_types_loops = {'UV'}
#   bpy.context.object.modifiers["DataTransfer"].loop_mapping = 'POLYINTERP_NEAREST'
#   bpy.context.object.modifiers["DataTransfer"].object = source
#   bpy.ops.object.datalayout_transfer(modifier="DataTransfer")
#   bpy.ops.object.modifier_apply(modifier="DataTransfer")

#   #changing to edit mode to retrieve voxel faces
#   bpy.ops.object.editmode_toggle()
#   bpy.ops.object.mode_set(mode = 'EDIT')
#   bpy.ops.mesh.select_mode(type='FACE')


#   #singularizing colours on each voxel face to look more animated
#   ob = bpy.context.active_object
#   bm = bmesh.from_edit_mesh(ob.data)
#   uv_lay = bm.loops.layers.uv.active

#   for face in bm.faces:
#       coords = [l[uv_lay].uv for l in face.loops]
#       x = (sum([uv[0] for uv in coords]) / len(coords))
#       y = (sum([uv[1] for uv in coords]) / len(coords))
#       for l in face.loops: l[uv_lay].uv = [x,y]

#   bmesh.update_edit_mesh(ob.data)

  #converting back to object mode for export
  bpy.ops.object.mode_set(mode = 'OBJECT')

  #deleting source and keeping target
  bpy.ops.object.select_all(action='DESELECT')
  source.select_set(True)
  bpy.ops.object.delete()

  #selecting target
  target.select_set(True)


  #exporting voxelized model
  output_directory = sourceDirectory
  vox_model_name = targetName + '.obj'
  output_file = os.path.join(output_directory, vox_model_name)
  print(output_file)
  bpy.ops.export_scene.obj(filepath=output_file)

print(args.input_path)
print(args.model_name)
print(args.output_model_name)
print(args.octree_depth)
print(args.scale)
voxelize(args.input_path, args.model_name, args.output_model_name, int(args.octree_depth), float(args.scale))